<?php

 /**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package     mwk-phpinfo
 * Copyright    (c) 2015 Agentur medienworx
 * @author      Peter Ongyert <peter.ongyert@medienworx.eu>
 * @license     http://www.gnu.org/licences/lgpl-3.0.html LGPL
 */

$GLOBALS['TL_LANG']['MOD']['mwk_phpinfo'][0] = 'PHP Info';
$GLOBALS['TL_LANG']['MOD']['mwk_phpinfo'][1] = 'PHP Info anzeigen';