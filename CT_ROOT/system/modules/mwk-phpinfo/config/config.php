<?php

 /**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package     mwk-phpinfo
 * Copyright    (c) 2015 Agentur medienworx
 * @author      Peter Ongyert <peter.ongyert@medienworx.eu>
 * @license     http://www.gnu.org/licences/lgpl-3.0.html LGPL
 */

$GLOBALS['BE_MOD']['mwk-manager']['mwk_phpinfo'] = array
(
    'callback'      =>      'ModulesPhpInfo',
    'icon'          =>      'system/modules/mwk-phpinfo/assets/images/phpinfo-icon.png',
    'stylesheet'    =>      'system/modules/mwk-phpinfo/assets/css/mwk-phpinfo.css'


);